---
title: "About"
---

I write software sometimes. I like forests, shooting, and books that are too old to get copies of anymore. I do other stuff too.

Email: [ymber@protonmail.com](mailto:ymber@protonmail.com).  
GPG key: [https://ymber.xyz/ymber.gpg](/ymber.gpg).
