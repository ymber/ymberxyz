---
title: "Reprinting Saint Basil's Address on Greek Literature"
date: 2022-03-31
draft: false
tags: ["books", "publishing", "classics"]
---

I've been working sporadically on my classical book revival project for a while. Muay Thai and lifting have been eating a lot of my time so progress on this has been mixed. I've all but finished another book and a Latin grammar poster. The website is coming on too but there have been a few rewrites and major feature revisions so it's taking a while.

Something I wanted a hard copy of that I couldn't easily come by was St Basil's Address on Greek Literature. The only English translation I find comfortably readable is Roy Deferrari's translation in the Loeb collection of St Basil's letters. It's hidden in thousands of pages of other letters in there and the formatting with the original Koine Greek side by side is awkward when you don't need that comparison so I put together a printing document for the translation of the address on its own. I had to bring the dimensions of this one down to 5.5"x8.5" from 6"x9" to meet the minimum page count for paperback binding and now I'm thinking that the smaller size feels better. I might experiment with printing the more substantial books in the same size.

It's a fairly unknown but quite historically significant document as it was the most influential of the Church fathers' addresses on pagan literature. I'm looking at printing some more letter-type documents from the early Church and possibly some compilations.

The printing documents are up on my GitLab and when the site is done you can get printed copies there.
