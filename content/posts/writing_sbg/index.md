---
title: "Writing a static site generator"
date: 2021-05-23
draft: false
tags: ["tools", "webdev", "linux"]
---

I needed a lightweight static site generator to generate these blog pages for me and the options were pretty thin. There's Roman Zolotarev's ssg but that doesn't generate RSS feeds and the templating options are too minimal even for me. Another other option available is blogit, which is closer to what I needed but I would rather write my own from scratch than extend a makefile based SSG. I forked it and tried but in the end I just emailed the patch I had back upstream and gave up. There are a few other options in this niche but they all quite similar in ways that make them unattractive for my use case.

This brings me to where I am now, writing my own static site generator from scratch. I'm calling it sbg for now but I might rename it if it expands beyond generating blogs. It's clean and minimal and it's written in POSIX standard sh. I took care to make sure it wouldn't hit problems with differences between GNU coreutils and the equivalent BSD tools as well. It will run on just about anything. I took significant inspiration from both ssg and blogit when I was designing this. The template system is quite similar to blogit's.

It isn't finished yet and there are still some limitations I need to deal with but it's good enough that I'm using it to generate the blog on this site. The main features I still need to add are generating RSS and Atom feeds and article tagging. I'm ambivalent about the tagging feature. I can see the use but I've never felt the need to use tags and it might end up as dead code.

The sbg repository is on my gitlab at [https://gitlab.com/ymber/sbg](https://gitlab.com/ymber/sbg).
