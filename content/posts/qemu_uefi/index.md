---
title: "Setup for UEFI in QEMU"
date: 2021-05-25
draft: false
tags: ["linux"]
---

I use QEMU for any virtualization I need and recently I needed to set up an Arch VM. Last year when I first worked out how to get an Arch guest working with a UEFI bootloader in QEMU it took a while to put the pieces of information together enough to get it working so now that I'm doing it again I'm documenting it here. There's not much to it once you work out how to do it the first time.

OVMF provides the necessary firmware files to enable UEFI boot in a QEMU guest. The files we need are the firmware executable `OVMF_CODE.fd` and the non-volatile variable store `OVMF_VARS.fd`. On Arch the package for OVMF is `edk2-ovmf` and the two files we need are found in `/usr/share/edk2-ovmf/x64` for x86_64 guests. The package name and possibly file locations will vary by distribution.

Before anything we need a virtual hard disk for the VM. The nature of this disk doesn't affect OVMF at all so define your virtual disk however you want. I'll be keeping all the VM files in `~/qemu` in this example.

```
qemu-img create -f raw arch_x64.raw 8G
```

Before setting up the VM we'll need a copy of the variable store for this VM. Per the [white paper](https://www.linux-kvm.org/downloads/lersek/ovmf-whitepaper-c770f8c.txt) VMs should not share variable stores.

```
cp /usr/share/edk2-ovmf/x64/OVMF_VARS.fd ~/qemu/arch_x64_vars.fd
```

To install the VM and later run it we need to expose the firmware executable and variable store to the guest. To do this we add a pair of virtual flash devices to the qemu command.

```
qemu-system-x86_64 -enable-kvm -m 2G \
                   -cdrom ~/dl/archlinux-2021.05.01-x86_64.iso -boot order=d -drive file=arch_x64.raw,format=raw \
                   -drive if=pflash,format=raw,readonly=on,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd \
                   -drive if=pflash,format=raw,file=arch_x64_vars.fd
```

The last two lines here are for OVMF. Note that the firmware executable is `readonly`. This command will boot the Arch installation image with OVMF allowing for a UEFI bootloader to be installed. You can install Arch as usual and the command to boot the VM afterwards is similar.

```
qemu-system-x86_64 -enable-kvm -m 2G \
                   -drive file=arch_x64.raw,format=raw \
                   -drive if=pflash,format=raw,readonly=on,file=/usr/share/edk2-ovmf/x64/OVMF_CODE.fd \
                   -drive if=pflash,format=raw,file=arch_x64_vars.fd
```

The qemu usage here only differs from BIOS mode usage by the two virtual drives we define for OVMF. I am setting up an Arch guest here but the same idea applies to whatever you're trying to boot.
